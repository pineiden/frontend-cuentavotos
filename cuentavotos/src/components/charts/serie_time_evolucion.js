import * as d3 from "d3"
import _ from 'lodash';


/*
Gráfico SerieTime de la evolución de los votos

*/
class ChartEvolucionVotos {

  constructor(opts) {
	this.refreshOpts(opts);
	this.createComponent();

	this.base_colors = [
			'#e41a1c',
			'#377eb8',
			'#4daf4a',
			'#984ea3',
			'#ff7f00',
			'#ffff33',
			'#a65628',
			'#f781bf',
			'#999999'];

	this.initTooltip()
  }

  createComponent() {
	let component = this.parent_component;
	if (!component){
	  this.chart =  this.selection
		.append(d3.creator("svg"));
	} else {
	  this.chart = this.parent_component.append(d3.creator('svg:g'));
	}
	this.chart.attr("width", this.width+"px")
	  .attr("height", this.height+"px")


	this.chart.append("g")
	  .attr("class",this.css_class)

    this.chart.append("g").attr("class","x axis").style("font-size","18px");
	this.chart.append("g").attr("class","y axis").style("font-size","18px"); 

	this.initTooltip();
  }


  refreshOpts(opts) {
	this.width = opts.width?opts.width:this.width;
	this.height = opts.height?opts.height:this.height;
	this.format_date = opts.format_date?opts.format_date:this.format_date;
	this.css_class = opts.css_class?opts.css_class:this.css_class;
	this.legends = opts.legends?opts.legend:this.legends;
	this.show_legends = opts.show_legends?opts.show_legends:this.show_legends;	
	this.parent_component = opts.parent_component?opts.parent_component:this.parent_component;
	this.parent_selector = opts.parent_selector?opts.parent_selector:this.parent_selector;
	this.margin = opts.margin?opts.margin:this.margin;
	this.colors = opts.colors?opts.colors:this.base_colors;
	this.selection = d3.select(this.parent_selector);
  }

  initChart(data, opts){
	if (opts){
	  this.refreshOpts(...opts);
	}
	this.refreshAxis(data);
	/* create the chart */
	// dates in Date type
	const x_translate = `translate(${this.margin.left},${this.height - this.margin.bottom})`
	
	this.chart
	  .selectAll("g.x.axis")
	.attr("transform",x_translate)
	.call(d3.axisBottom(this.xAxis));

	const y_translate = `translate(${this.margin.left*2},0)` 
	this.chart
	  .selectAll("g.y.axis")
	.attr("transform",y_translate)
	.call(d3.axisLeft(this.yAxis));
	let locale = d3.formatDefaultLocale(this.format_date);
	this.chart.dateformat = d3.timeFormat(locale)
	this.chart
	  .selectAll("g.line")


	this.chart
	  .append("g").attr("id","circles")
	this.chart
	  .append("g").attr("id","labels")

	this.putLegends()
  }

  putLegends(){

	if (!_.isEmpty(this.legends)){
	  const x_translate = `translate(${this.margin.left},${this.height - this.margin.bottom})`

		if (this.legends.x){
		  console.log("Adding xlabel", this.legends.x)
		this.chart.select("#labels").append("text")
			.attr("class", "x label")
			.attr("text-anchor", "end")
			.attr("x", this.width)
			.attr("y", this.height - 6)
			.text(this.legends.x);
		}

		if (this.legends.y){
		  console.log("Adding ylabel", this.legends.x)

		this.chart.select("#labels").append("text")
		.attr("class", "y label")
		.attr("x", this.width)
		.attr("y", this.height - 6)
		.attr("text-anchor", "end")
		.attr("y", 6)
		.attr("dy", ".75em")
		.attr("transform", "rotate(-90)")
		.text(this.legends.y);}
	}	

  }

  refreshAxis(data){	
	let start_dates = data.map(item => {
	  return item.votos[0].dt_gen});
	let end_dates = data.map(item => item.votos[item.votos.length - 1].dt_gen);
	
    const dates = [d3.min(start_dates), d3.max(end_dates)];
	let min_value = d3.min(data.map(dataitem => {
	  let item = dataitem;
	  let keyNames = Object.keys(item);
	  if (dataitem.votos) {
		return d3.min(dataitem.votos.map(item=>item.cantidad))
	  }
	}))* .95;
	let max_value = d3.max(data.map(dataitem => {
	  let item = dataitem;
	  let keyNames = Object.keys(item);
	  if (dataitem.votos) {
		return d3.max(dataitem.votos.map(item=>item.cantidad))
	  }
	}))* 1.05;
	const xLimits = [this.margin.left,this.width-3*this.margin.right];
	let xAxis = d3.scaleTime()
	.domain(dates)
	.range(xLimits)
	const yLimits = [this.height-this.margin.bottom,1.2*this.margin.top];
	let yAxis = d3.scaleLinear().domain([min_value, max_value]).range(yLimits);
	this.xAxis = xAxis;
	this.yAxis = yAxis;
	/* put labels t axis */
  }
  
  initTooltip(){
  this.chart.tooltip = d3.select(this.parent_selector)
    .append("div")
	.attr("id","tooltip")
	.style("font-size","16px")
	.style("position", "absolute")
    .style("opacity", 0)
    .attr("class", "tooltip")
    .style("background-color", "white")
    .style("border", "solid")
    .style("border-width", "2px")
    .style("border-radius", "5px")
    .style("padding", "5px")
    .style("text-align", "left")
  }

  updateChart(data, opts) {
	/* insert new data and draw 
	   data is  
       [
	   {norma1 data1},
	   {norma2 data2}
	   ]
	 */

	   // agrupa por norma id
	   // lista los elementos por id, cuantas normas se graficas
	let colors = [...this.colors]
	//    console.log("res", res, "BASE COLORS",colors)
	this.chart.selectAll(".line").remove()
	this.chart.selectAll(".dot-voto").remove()

	if (!_.isEmpty(opts)){
	  this.refreshOpts(opts);
	}else{
	  this.refreshOpts(opts);
	}
	this.refreshAxis(data);
	if (data.length > 0) {
	   const dataset = d3.index(data, item => {
		 if (item.norma){
		   return item.norma.id
		 }
	   });
      const res = [...dataset.keys()];
	//    // se define la fn de color
	   const color = d3.scaleOrdinal()
		   .domain(res).range(this.colors);

	  const x_translate = `translate(${this.margin.left},0)`
	  this.chart.selectAll(".line")
	  .data(dataset)
		.join("path")
	    .attr("fill", "none")
	    .attr("stroke", ([key, array])=>{
			return color(key)
		  })
	      .attr("stroke-width", 1.5)
	      .attr("d", ([key, array]) => {
			let line = d3.line()
			.x( (item) => {
			  //console.log("Item", item, typeof item.dt_gen,
			  //this.xAxis)
			  let result = this.xAxis(item.dt_gen)
			  return result
			})
			.y((item) => {
			  let result = this.yAxis(item.cantidad)
			  return this.yAxis(item.cantidad)
			})(array.votos);
			return line
		  })
		.attr("class","line")
		.attr("transform", x_translate);

	  let circles = {};

	  let chart = this.chart
	  let xAxis = this.xAxis
	  let yAxis = this.yAxis
	  let dtFormat = d3.timeFormat("%d/%m/%Y %H:%M")

  // create a tooltip
  let Tooltip = this.chart.tooltip;

  const mouseover = function(event, item) {
	let date_str = dtFormat(item.dt_gen);

    d3.select(this)
      .style("stroke", "blue")
      .style("stroke-width", 2)
      .style("opacity", .4)

    Tooltip
      .style("opacity", 1)
  }

  const mousemove = function(event, item) {
	let date_str = dtFormat(item.dt_gen);
    Tooltip      
      .html(`
       <p>Fecha: ${date_str}</p>
	   <p>Votos: ${item.cantidad}</p>`)
      .style("left",()=>{ 
		const x = event.layerX?event.layerX:event.screenX;
		return (x - 30) + "px"})
      .style("top", ()=>{
		const y = event.layerY?event.layerY:event.screenY;
		return (y + 100) + "px"})
  }

  const mouseleave = function(event, item) {
    Tooltip
      .style("opacity", 0)
    d3.select(this)
      .attr("stroke", "#6644b5")
      .style("opacity", 0.2)
  }

	  for (let key of res) {
		  this.chart
		  .selectAll("#circles")
		  .data(dataset.get(key).votos)
		  .join("circle")
		  .attr("r",5)
		  .attr("cx",(item)=>{
			return this.xAxis(item.dt_gen)
		  })
		  .attr("cy",(item) => {
			return this.yAxis(item.cantidad)
		  }).attr("transform", x_translate)
		  .attr("fill", "#ffcab0")
		  .style("opacity", .2)
		  .attr("stroke", "#6644b5")
		  .attr("stroke-width", 1)
		  .attr("class", "dot-voto")
		  .on("mouseover",  mouseover)
		  .on("mousemove",  mousemove)
		  .on("mouseout", mouseleave);
	  }
	  // .append("circle", ([key, array]) => {
	  // 	console.log("Circle to key", key, array)
	  // 	let circles = d3
	  // 		.create("g")
	  // 		.attr("class",".circles")
	  // 		.append("circle")	  
	  // 		.attr("r", 3)
	  // 		.attr("cantidad", (item) => {
	  // 		  item.cantidad})
	  // 		.attr("dt_gen", (item) => item.dt_gen)
	  // 		.attr("fill", (item) => color(item.id))	  
	  // 		.attr("cx",(item) => {
	  // 		  console.log("add circle", item,this.xAxis(item.dt_gen))
	  // 		  return this.xAxis(item.dt_gen)
	  // 		})
	  // 		.attr("cy",(item)  => {
	  // 		  return this.yAxis(item.cantidad)
	  // 		})	  
	  // 		.on("mouseover",(item) => {
	  // 		  return this.tooltip.style("visibility", "visible")
	  // 		})
	  // 		.on("mousemove", mousemove)
	  // 		.on("mouseout",(item) => {
	  // 		  return this.tooltip.style("visibility", "hidden")
	  // 		})(array.votos);
	  // 	console.log("Circulo a entregar", circles)
	  // 	return circles
	  // });

	}//if
  }//method
}// class


export default ChartEvolucionVotos
