export default class Voto {
  constructor(voto) {
	this.id = voto.id
	this.dt_gen = new Date(voto.dt_gen)
	this.cantidad = voto.cantidad
	this.delta = voto.delta
	this.tasa = voto.tasa
  }
}
