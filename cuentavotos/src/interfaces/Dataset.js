import Norma from './Norma.js'
import Voto  from './Voto.js'

export default class DatasetNorma {
  constructor(norma, votos){
  this.norma = norma
  this.votos = votos
  }

  get norma() {
	return this.norma 
  }

  get votos() {
	return this.votos
  }
}
