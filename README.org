#+TITLE: Documentación Frontend Cuentavotos

* Introduction

Esta aplicación tiene como objetivo exponer una interfaz para el
monitoreo de las propuestas de normas constituyentes.

Tecnologías que ocupa.

- Javascript
- Vue 3 
- D3 
- Axios 
- Lodash

La interfaz mantiene las siguientes características principales.

- Una tabla con todas las normas monitoreadas.
- Un filtro para normas 
- Un seccion de gráficos de norma seleccionada.

Para desarrollar o hacer el paquete final, necesitas instalar las
dependencias.

#+begin_src shell 
npm install
#+end_src

Luego, para desarrollar, frente a los cambios actualizará la pantalla.

#+begin_src shell 
npm run dev
#+end_src

Finalmente, para hacer el paquete final.

#+begin_src shell 
npm run build
#+end_src

* Configuración base.

Para iniciar, debes entregar los datos de host de conexión. Hazlo seteando la
variable de ambiente `VITE_CUENTAVOTOS_URL` a la dirección del API, también
puedes hacerlo con un archivo `.env` en `cuentavotos/`. Por defecto es
`http://127.0.0.1:8000`.

Luego, entregar un diccionario (json) con los valores que definen la *api* para
las normas y votos. Según la fuente, podría cambiar. Por defecto es
`{normas:"normas", votos:"votos"}`. Si deseas cambiarlo tendrás que setearlo en
el componente mismo, en el archivo `App.vue`.

#+begin_src javascript
<CuentaVotos 
    :url="http://127.0.0.1:8000/"
    :api="{normas:"normas", votos:"votos"}"
/>
#+end_src

* Estructura de archivos

Se mantiene una estructura de archivos jerarquizada en un orden de
interfaces, componentes y gráficos.

Los componentes son archivos *vue*, los gráficos son archivos *js* con
clases que crean los dibujos a graficar, las interfaces son aquellas
*clases* de los elementos principales como *Norma* y *Voto*.

#+name: estructura
#+begin_src shell :results output
tree ./cuentavotos/src
#+end_src

La estructura resultante del proyecto viene a ser la siguiente:

#+RESULTS: estructura
#+begin_example
./cuentavotos/src
├── App.vue
├── assets
│   └── logo.png
├── components
│   ├── CuentaVotos.vue
│   ├── ErrorComponent.vue
│   ├── Filtro.vue
│   ├── Grafico.vue
│   ├── LoadingComponent.vue
│   ├── Modal.vue
│   ├── Normas.vue
│   ├── charts
│   │   ├── Circle.vue
│   │   ├── serie_time_delta.js
│   │   ├── serie_time_delta.ts
│   │   ├── serie_time_evolucion.js
│   │   └── serie_time_tasa.js
│   ├── ck.html
│   └── normas.vue
├── index.js
├── interfaces
│   ├── Dataset.js
│   ├── Norma.js
│   └── Voto.js
├── main.js
└── use
    └── resizeObserver.js

5 directories, 22 files
#+end_example

En simple, la estructura de directorios será.

#+name: estructura-dir
#+begin_src shell :results output
tree -d ./cuentavotos/src
#+end_src

Mantiene solo dos niveles de jerarquía.

#+RESULTS: estructura-dir
: ./cuentavotos/src
: ├── assets
: ├── components
: │   └── charts
: ├── interfaces
: └── use
: 
: 5 directories

 
* Configuración NGINX

Para facilitar las cosas, puedes usar la siguiente configuración para
el *proxy nginx*.

Esta permitirá compartir la API REST en modo seguro. Debes cambiar los
valores <HOST>,  <DOMINIO> y <RUTA>

#+begin_src nginx
server {
    listen <HOST>;
    server_name votos.<DOMINIO>;

    location / { return 301 https://$host$request_uri; }
}

server {
    listen <HOST>:443 ssl http2;
    server_name votos.<DOMINIO>;

    access_log /var/log/nginx/votos.<DOMINIO>.access.log;
    error_log  /var/log/nginx/votos.<DOMINIO>.error.log;

    # Certbot certificates
    ssl_certificate /etc/letsencrypt/live/votos.eradelainformacion.cl/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/votos.eradelainformacion.cl/privkey.pem;

    location /api/ {
        proxy_pass http://127.0.0.1:8000/;
        proxy_set_header Host $http_host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
    }

    location / {
        root <RUTA>/cuentavotos/frontend;
        index index.html;
    }
}
#+end_src
